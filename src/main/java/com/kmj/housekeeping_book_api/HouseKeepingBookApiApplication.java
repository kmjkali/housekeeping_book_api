package com.kmj.housekeeping_book_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HouseKeepingBookApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HouseKeepingBookApiApplication.class, args);
    }

}

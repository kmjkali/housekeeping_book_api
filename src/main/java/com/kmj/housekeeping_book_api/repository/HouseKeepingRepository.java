package com.kmj.housekeeping_book_api.repository;

import com.kmj.housekeeping_book_api.entity.HouseKeeping;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HouseKeepingRepository extends JpaRepository<HouseKeeping,Long> {
}

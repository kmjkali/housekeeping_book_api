package com.kmj.housekeeping_book_api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HouseKeepingRequest {
    private String inOut;
    private Integer price;
    private String etcMemo;
}

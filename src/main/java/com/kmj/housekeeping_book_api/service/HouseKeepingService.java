package com.kmj.housekeeping_book_api.service;

import com.kmj.housekeeping_book_api.entity.HouseKeeping;
import com.kmj.housekeeping_book_api.model.HouseKeepingRequest;
import com.kmj.housekeeping_book_api.repository.HouseKeepingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HouseKeepingService {
    private final HouseKeepingRepository houseKeepingRepository;

    public void setHouseKeeping(HouseKeepingRequest request){
        HouseKeeping addData =new HouseKeeping();

        addData.setDateCreate(LocalDate.now());
        addData.setInOut(request.getInOut());
        addData.setPrice(request.getPrice());
        addData.setEtcMemo(request.getEtcMemo());

        houseKeepingRepository.save(addData);


    }
}

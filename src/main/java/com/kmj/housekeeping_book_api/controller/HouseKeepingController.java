package com.kmj.housekeeping_book_api.controller;

import com.kmj.housekeeping_book_api.model.HouseKeepingRequest;
import com.kmj.housekeeping_book_api.service.HouseKeepingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/house")
public class HouseKeepingController {
    private final HouseKeepingService houseKeepingService;

    @PostMapping("/keeping")
    public String setHouseKeeping(@RequestBody HouseKeepingRequest request){
        houseKeepingService.setHouseKeeping(request);

        return "집에좀 가자아!";
    }
}

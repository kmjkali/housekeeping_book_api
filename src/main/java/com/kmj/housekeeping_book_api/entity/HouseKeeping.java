package com.kmj.housekeeping_book_api.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class HouseKeeping {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false, length = 20)
    private String inOut;

    @Column(nullable = false)
    private Integer price;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;


}
